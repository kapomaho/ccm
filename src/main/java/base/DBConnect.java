package base;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DBConnect {
	Connection connection;
	private Statement statement;

	public Connection getConnection() throws SQLException,
			ClassNotFoundException, FileNotFoundException, IOException {

		String driverName = "oracle.jdbc.driver.OracleDriver";
		Class.forName(driverName);
		//String configFileLocation = String.format("%s%ssrc%smain%sresources%sconfig.properties", System.getProperty("user.dir"), File.separator, File.separator, File.separator, File.separator);

		Properties prop = new Properties();
		prop.load(new FileInputStream("config.properties"));
		String server = prop.getProperty("server");
		String user = prop.getProperty("dbuser");
		String password = prop.getProperty("dbpassword");
		String serviceName=prop.getProperty("serviceName");
		String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST="
				+ server
				+ ")(PORT=1521)) (CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME="+serviceName+")))";
		connection = DriverManager.getConnection(url, user, password);

		return connection;

	}

	

	public ResultSet getAllResult(String query) throws SQLException {
		statement = connection.createStatement();
		ResultSet res = statement.executeQuery(query);
		return res;

	}

	// Gönderilen queryinin ilk rowunu geriye dönderir
	public ResultSet getFirstRowResult(String query) throws SQLException {
		statement = connection.createStatement();
		statement.setMaxRows(1);
		ResultSet res = statement.executeQuery(query);
		return res;
	}

	// Gönderilen queryinin ilk rowunun string typeli colonunu gönderir
	public String getFirstRowResultCellValue(String query, String columnName)
			throws SQLException {
		statement = connection.createStatement();
		statement.setMaxRows(1);
		ResultSet res = statement.executeQuery(query);
		res.next();

		return res.getString(columnName);
	}

	// Gönderilen queryinin ilk rowunun int type li colonunun degerini gönderir
	public int getFirstRowResultCellValue(String query, int columnName)
			throws SQLException {
		statement = connection.createStatement();
		statement.setMaxRows(1);
		ResultSet res = statement.executeQuery(query);
		res.next();

		return res.getInt(columnName);
	}

	public void disConnect() throws SQLException {

		connection.close();
	}
	
	
	public List getMailId(String subject) throws SQLException, ClassNotFoundException, FileNotFoundException, IOException
	{
		getConnection();

		String query="select * from MAIL where subject=?";
        PreparedStatement  preparedStatement = connection.prepareStatement(query);
		preparedStatement.setString(1, subject);
        List<Integer> list = new ArrayList<Integer>();

		int id =0;
		int mailStatus=0;
		ResultSet rs = preparedStatement.executeQuery();

		while (rs.next()) {

			 id =rs.getInt("ID");
			 mailStatus= rs.getInt("MAIL_STATUS_ID");
			list.add(id);
			list.add(mailStatus);
			//System.out.println(id);
			//System.out.println(mailStatus);

		}
		
			rs.close();
			disConnect();
		return list;
	}
	
	public int kapamaKontrol(int bildirim) throws Exception{	
		getConnection();
		String query="select count(*) as total from ttsys_bildirim where ID='"+bildirim+"'";
		//statement=connection.createStatement();
		Statement stmt = connection.createStatement();
		 ResultSet rs = stmt.executeQuery(query);
		 	rs.next();
		 int count=rs.getInt("total");
		
		return count;
		
	
	}
	
	
	public int kapamaKontrolArch(int bildirim) throws Exception{	
		getConnection();
		String query="select count(*) as total from ttsys_bildirim_arch where ID='"+bildirim+"'";
		//statement=connection.createStatement();
		Statement stmt = connection.createStatement();
		 ResultSet rs = stmt.executeQuery(query);
		 	rs.next();
		 int count=rs.getInt("total");
		
		return count;
		
	
	}
	
	
	public int getBildirim(int mail) throws SQLException, ClassNotFoundException, FileNotFoundException, IOException
	{
		getConnection();
		String query="select * froM TTSYS_BILDIRIM where MAIL_ID=?";
        PreparedStatement  preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1, mail);

		int bildirim=0;
		ResultSet rs = preparedStatement.executeQuery();

		while (rs.next()) {

			 bildirim =rs.getInt("ID");
	
		}
		
			rs.close();
			disConnect();
		return bildirim;
	}
	
	public String getSubject(int bildirims) throws SQLException, ClassNotFoundException, FileNotFoundException, IOException
	{
		getConnection();
		String query="select A.SUBJECT from MAIL A,TTSYS_BILDIRIM B WHERE A.ID=B.MAIL_ID AND B.ID=?";
        PreparedStatement  preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1, bildirims);

		String bildirim="";
		ResultSet rs = preparedStatement.executeQuery();

		while (rs.next()) {

			 bildirim =rs.getString("SUBJECT");
	
		}
		
			rs.close();
			disConnect();
		return bildirim;
	}

}
