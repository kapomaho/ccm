package base;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.activation.*;

public class SentMail extends AltYapi{
	 String host;
	 String user;
	 String pass;
	 String SSL_FACTORY;
	//String[] to = {"mahir.kaplanci@etiya.com","volkan.erleblebici@etiya.com","ersoy.koktas@etiyacom"};
	String[] to = {""};
	 String from;
	 String subject;
	 File file;
	boolean sessionDebug = true;
	PrintWriter printer;
	FileWriter writer;
	public SentMail() throws FileNotFoundException, IOException
	{
		user=getConf("mailKullanici");
		pass=getConf("mailKullaniciSifre");
		 host = "smtp.gmail.com"; //"smtp.gmail.com";
		 //user = "ccmtestotomasyonu@gmail.com"; //email idin
		// pass = "telauraccm2017"; 
		SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		to[0]=getConf("mailIssDogan");
		 from = "ccmtestotomasyonu@gmail.com"; //
		 subject = "subject";
		// messageText = "Geldimi \r\nGeldiyse iyi";
	}
	 
	public int sendMail(String konu,String messageText,String fileBody) throws AddressException, MessagingException, IOException
	{
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		props.put("mail.transport.protocol.", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
		Session mailSession = Session.getDefaultInstance(props, null);
		//mailSession.setDebug(sessionDebug);
		MimeMessage  msg = new MimeMessage(mailSession);
		msg.setFrom(new InternetAddress(from));
		 InternetAddress[] addressTo = new InternetAddress[to.length];
         for (int i = 0; i < to.length; i++)
         {
             addressTo[i] = new InternetAddress(to[i]);
         }
       //  msg.setRecipients(RecipientType.TO, addressTo); 
         msg.addRecipients(RecipientType.TO, addressTo);
		msg.setSubject(konu);
		
		
		BodyPart messageBodyPart = new MimeBodyPart();
		 messageBodyPart.setText(messageText);
		 Multipart multipart = new MimeMultipart();
		  multipart.addBodyPart(messageBodyPart);
		  messageBodyPart = new MimeBodyPart();
	         String filename = "EK.txt";
	         this.file = new File(filename);   
	         if(file.exists())
	         {
	        	 file.delete();
	         }
	         file.createNewFile();
	         this.writer = new FileWriter(this.file, true);
				this.printer = new PrintWriter(this.writer);
				
				printer.write(fileBody);
				this.writer.close();
				this.printer.close();
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(filename);
	         multipart.addBodyPart(messageBodyPart);
	         msg.setContent(multipart );
		//msg.setContent(messageText, "text/plain"); // 
	         int returnValue=0;
		Transport transport = mailSession.getTransport("smtp");
		transport.connect(host, user, pass);
		try {
		transport.sendMessage(msg, msg.getAllRecipients());
		returnValue=1;
		System.out.println("Email sent");

		}
		catch (Exception err) {
		}
		
		transport.close();	
		
		return returnValue;
	}
	
	}
	

