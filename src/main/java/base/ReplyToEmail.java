package base;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SearchTerm;

public class ReplyToEmail {
   
	String host;
	String user;
	String pass;
	String SSL_FACTORY;
	
	public ReplyToEmail()
	{
		 host = "smtp.gmail.com"; //"smtp.gmail.com";
		  user = ""; //email idin
		 pass = ""; 
		 SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
         int returnValue=0;

	}
	
	public int sendMail(String user,String password,final String konu,String messageText,String cc) throws AddressException, MessagingException, IOException
	{
      Date date = null;
      int x=0;

      Properties properties = new Properties();
      properties.put("mail.store.protocol", "pop3");
      properties.put("mail.pop3s.host", "pop.gmail.com");
      properties.put("mail.pop3s.port", "995");
      properties.put("mail.pop3.starttls.enable", "true");
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.smtp.socketFactory.fallback", "false");
      properties.put("mail.smtp.socketFactory.class", SSL_FACTORY);
      properties.put("mail.smtp.port", "465");
      Session session = Session.getDefaultInstance(properties);
      try 
      {
         Store store = session.getStore("pop3s");
         store.connect("pop.gmail.com", user,
            password);//change the user and password accordingly

         Folder folder = store.getFolder("inbox");
         if (!folder.exists()) {
            System.out.println("inbox not found");
               System.exit(0);
         }
         folder.open(Folder.READ_ONLY);

        

         // creates a search criterion
         SearchTerm searchCondition = new SearchTerm() {
             @Override
             public boolean match(Message message) {
                 try {
                     if (message.getSubject().endsWith(konu)||message.getSubject().startsWith(konu)) {
                         return true;
                     }
                 } catch (MessagingException ex) {
                     ex.printStackTrace();
                 }
                 return false;
             }
         };
         
         

         // performs search through the folder
         Message[] foundMessages = folder.search(searchCondition);
        
         if (foundMessages.length != 0) {
        	 
        	 x=1;

               Message message = foundMessages[0];
               date = message.getSentDate();
               // Get all the information from the message
               String from = InternetAddress.toString(message.getFrom());
               if (from != null) {
                  System.out.println("From: " + from);
               }
               String replyTo = InternetAddress.toString(message
	         .getReplyTo());
               if (replyTo != null) {
                  System.out.println("Reply-to: " + replyTo);
               }
               String to = InternetAddress.toString(message
	         .getRecipients(Message.RecipientType.TO));
               if (to != null) {
                  System.out.println("To: " + to);
               }

               String subject = message.getSubject();
               if (subject != null) {
                  System.out.println("Subject: " + subject);
               }
               Date sent = message.getSentDate();
               if (sent != null) {
                  System.out.println("Sent: " + sent);
               } 
               

                  Message replyMessage = new MimeMessage(session);
                  replyMessage = (MimeMessage) message.reply(false);
                  replyMessage.setFrom(new InternetAddress(to));
                  replyMessage.setText("Thanks");
                  
                  if(cc.compareTo("")!=0)
                  {
                  replyMessage.addRecipient(RecipientType.CC, new InternetAddress(cc));
                  }
                  replyMessage.setReplyTo(message.getReplyTo());
                  Transport t = session.getTransport("smtp");
                  try {
	   	    
              		t.connect(host, user, password);
	             t.sendMessage(replyMessage,
                        replyMessage.getAllRecipients());

                  } finally {
                     t.close();
                  }
                  System.out.println("message replied successfully ....");

                  // close the store and folder objects
                  folder.close(false);
                  store.close();
            }//end of for loop

         

      } catch (Exception e) {
         e.printStackTrace();
      }
      
  	return x;

	}
	

}