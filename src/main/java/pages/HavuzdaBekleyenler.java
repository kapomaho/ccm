package pages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class HavuzdaBekleyenler extends base.AltYapi{
	
public WebDriver driver;
public WebElement element;

@FindBy(xpath="//*[contains(@name,'havuzListesiForm:taskHvzTable:215')]")
public WebElement selectedCheckbox;

@FindBy(id="havuzListesiForm:taskHvzTable:refreshGenelHavuzList")
public WebElement refresh;

@FindBy(id="havuzListesiForm:taskHvzTable:assignTaskToUser")
public WebElement kisiyeAtama;

@FindBy(xpath="//*[contains(text(),'Talep Durumu')]")
public WebElement talepDurumu;

@FindBy(id="havuzListesiForm:taskHvzTable:bildirimNoId")
public WebElement bildirimSearchbox;
public JavascriptExecutor js;
public HavuzdaBekleyenler(WebDriver driver)	{
	this.driver=driver;
	 js = (JavascriptExecutor) driver;
	wait =new WebDriverWait(driver,50);
}	


public void bildirimArama(int bildirim) throws InterruptedException
{	
	xpathParcasiMesajKontrolEt(talepDurumu, "Talep Durumu");

	write(bildirimSearchbox,String.valueOf(bildirim));
	js.executeScript("document.body.style.zoom='60%'");
	beklemeSuresiBelirt(2000);//
	js.executeScript("arguments[0].click();", refresh);
	//click();
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(),'"+bildirim+"')]")));
	beklemeSuresiBelirt(2000);
		 
	
}
	
public AtamaBilgileri kisiAtamaFromPool(int bildirim) throws InterruptedException
{
	
	bildirimArama(bildirim);
	bildirimSelectedCheckbox();
	beklemeSuresiBelirt(1000);
	js.executeScript("arguments[0].click();", kisiyeAtama);
	return PageFactory.initElements(driver, AtamaBilgileri.class);
}
public void bildirimSelectedCheckbox() throws InterruptedException
{
	js.executeScript("arguments[0].click();", selectedCheckbox);
	
}
public AtamaBilgileri kisiAtama(int bildirim) throws InterruptedException
{
	
	bildirimArama(bildirim);
	bildirimSelectedCheckbox();
	beklemeSuresiBelirt(1000);
	js.executeScript("arguments[0].click();", kisiyeAtama);
	return PageFactory.initElements(driver, AtamaBilgileri.class);
}
}
