package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class HomePage extends base.AltYapi {
	
	public WebDriver driver;
	public WebElement element;
	
	@FindBy(xpath="//*[contains(text(),'Bildirim Bilgileri')]")
	public WebElement bildirimBilgileri;
	
	
	
	
	@FindBy(linkText="Bildirim ��lemleri")
	public WebElement bildirimIslemleri;
	
	
	
	@FindBy(xpath="//*[contains(text(),'Kisisel Havuz Listesi')]")
	public WebElement kisiselHavuzListesi;
	

	@FindBy(xpath="//*[contains(text(),'Genel Havuz Listesi')]")
	public WebElement genelHavuzListesi;
	
	@FindBy(xpath="//*[contains(text(),'Bildirim Sorgulama')]")
	public WebElement bildirimSorgulamaLink;

	
	@FindBy(xpath="//*[contains(text(),'Atamas� Yap�lan ��ler')]")
	public WebElement atamasiYapilanIsler;


	@FindBy(xpath="//*[contains(text(),'Havuzda Bekleyenler')]")
	public WebElement havuzdaBekleyen;
	
	
	public HomePage(WebDriver driver)	{
		this.driver=driver;
		wait =new WebDriverWait(driver,10);
	}	
	
	public BildirimSorgulama bildirimSorgulama() throws InterruptedException
	{
		driver.switchTo().defaultContent();
		click(bildirimSorgulamaLink);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
		beklemeSuresiBelirt(1000);
		changeFrame();
		return PageFactory.initElements(driver, BildirimSorgulama.class);

	}
	
public BildirimIslemleri bildirimGenelEkran() throws InterruptedException
{
	click(bildirimBilgileri);
	click(bildirimIslemleri);
	changeFrame();
	return PageFactory.initElements(driver, BildirimIslemleri.class);
}
	
	public KisiselHavuzListesi navigateKisiselHavuz() throws InterruptedException
	{
		click(bildirimBilgileri);
		click(kisiselHavuzListesi);
		changeFrame();
		return PageFactory.initElements(driver, KisiselHavuzListesi.class);//D
	}
	

	
	public AtamasiYapilanIsler navigateAtamasiYapilan() throws InterruptedException
	{
		click(bildirimBilgileri);
		click(genelHavuzListesi);
		click(atamasiYapilanIsler);
		changeFrame();
		return PageFactory.initElements(driver, AtamasiYapilanIsler.class);//D
	}
	
	
	public HavuzdaBekleyenler navigateAtamasiYapilanHavuz() throws InterruptedException
	{
		click(bildirimBilgileri);
		click(genelHavuzListesi);
		click(havuzdaBekleyen);
		changeFrame();
		return PageFactory.initElements(driver, HavuzdaBekleyenler.class);//D
	}
	
}
