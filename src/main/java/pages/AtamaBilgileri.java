package pages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class AtamaBilgileri extends base.AltYapi{
	
public WebDriver driver;
public WebElement element;
public JavascriptExecutor js;
//	Bildirim(ler) kullan�c�ya atand�. 

@FindBy(xpath="//*[text()='Atama Bilgileri']")
public WebElement atamaBilgileriSayfasi;

@FindAll({
	@FindBy(xpath="//*[contains(@id,'seciliIslerFormId:kullaniciAramaResultTable:0')]"),
	@FindBy(xpath="//*[text()='Ata']")
})
public WebElement ataButton;

//seciliIslerFormId:kullaniciAramaResultTable:0:j_id257
//@FindBy(xpath="//*[contains(@id,'seciliIslerFormId:kullaniciAramaResultTable:0:j_id')]")
@FindBy(xpath="//input[contains(@id,'kullaniciAramaResultTable:0:j_id')]")
public WebElement atas;

@FindBy(xpath="//*[contains(text(),'Kullan�c� Ad�')]/following::input[1]")
public WebElement kullaniciAdi;

@FindBy(xpath="//*[contains(text(),'Bildirim(ler) kullan�c�ya atand�.')]")
public WebElement bildirimAtandiSucessMessage;

//seciliIslerFormId:kullaniciAramaResultTable

@FindBy(id="seciliIslerFormId:kullaniciAramaResultTable")
public List<WebElement> tableResult;


@FindBy(xpath="//*[contains(@id,'submitButon')]")
public WebElement araButton;

public AtamaBilgileri(WebDriver driver)	{
	this.driver=driver;
	 js = (JavascriptExecutor) driver;
	wait =new WebDriverWait(driver,10);
}	



public void kullaniciAtamaFromHavuz() throws InterruptedException
{
	js.executeScript("document.body.style.zoom='100%'");

	xpathParcasiMesajKontrolEt(atamaBilgileriSayfasi, "Atama Bilgileri");
	write(kullaniciAdi, "issasistan2");
	click(araButton);
	//js.executeScript("document.body.style.zoom='80%'");

	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	int rowCount=tableResult.size();
	System.out.println("Sat�r say�s�"+rowCount);
	//click(atas);
	beklemeSuresiBelirt(1000);
	((JavascriptExecutor) driver).executeScript("arguments[0].click();", atas);
	//click(ataButton);
	beklemeSuresiBelirt(1000);
	xpathParcasiMesajKontrolEt(bildirimAtandiSucessMessage, "Bildirim(ler) kullan�c�ya atand�.");
		
}


}
