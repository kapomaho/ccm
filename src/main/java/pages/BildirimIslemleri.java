package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Screen;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class BildirimIslemleri extends base.AltYapi{
	
public WebDriver driver;
public WebElement element;

@FindBy(xpath="//*[text()='Ekle']")
public WebElement dosyaEkle;

//mail gonder emailGonderReplyForm:j_id1604

@FindBy(xpath="//input[contains(@id,'emailGonderReplyForm:j_id')]")
public WebElement mailGonder;


@FindBy(xpath="//*[text()='Geri']")
public WebElement geri;

@FindBy(xpath="//*[text()='Y�kle']")
public WebElement yukle;

@FindBy(id="emailGonderReplyForm:to")
public WebElement to;

@FindBy(name="customerOperationsForm:customerInfoForm:customerCreateTicketForm:single_servicesNumber")
public WebElement hizmetNo;

@FindBy(id="customerOperationsForm:customerInfoForm:customerCreateTicketForm:bilAlKanalPanel")
public WebElement gelisKanali;

@FindBy(xpath="//*[contains(text(),'Birim Ad�')]/following::select[1]")
public WebElement birimAdi;

@FindBy(xpath="//*[contains(text(),'Grup Ad�')]/following::select[1]")
public WebElement grupAdi;


@FindBy(id="mainbildirimGorevCikisModalPanelForm:kapamaKoduGrupList")
public WebElement kapamaKoduGrubuCombo;


@FindBy(id="mainbildirimGorevCikisModalPanelForm:kapamaKoduList")
public WebElement kapamaKoduCombo;


@FindBy(id="ticketInformationForm:islemCombo")
public WebElement islemCombo;


@FindBy(xpath="//*[contains(text(),'A��klamalar')]")
public WebElement aciklamalarLink;

@FindBy(xpath="//div[contains(text(),'Bildirim Bilgileri')]")
public WebElement bildirimBilgileri;

@FindBy(xpath="//*[contains(text(),'M��teri ��lemleri Ekran�')]")
public WebElement musteriIslemleriEkrani;

@FindBy(xpath="//*[contains(text(),'Dosyalar')]")
public WebElement dosyalarLink;


@FindBy(xpath="//*[contains(text(),'Yard�m Masas�')]")
public WebElement yardimMasasiLink;

@FindBy(xpath="//*[contains(text(),'�leti�im Ge�mi�i')]")
public WebElement iletisimGecmisiLink;

@FindBy(xpath="//div[contains(text(),'Bildirim ��lemleri')]")
public WebElement bildirimIslemleriLink;

@FindBy(id="ticketInformationForm:islemCombo")
public WebElement gorevCikisiCombo;

@FindBy(id="mainbildirimGorevCikisModalPanelForm:j_id1474")
public WebElement gorevCikisiPanelButton;


@FindBy(id="ticketInformationForm:islemCombo")
public WebElement gorevCikisCombo;

@FindBy(id="mainbildirimGorevCikisModalPanelForm:kapamaKoduGrupList")
public WebElement kapamaKoduGrubu;

@FindBy(id="mainbildirimGorevCikisModalPanelForm:kapamaKoduList")
public WebElement kapamaKodu;

@FindBy(id="mainbildirimGorevCikisModalPanelForm:bilidirimGorevCikisModalButton")
public WebElement gerceklestir;


@FindBy(id="customerOperationsForm:customerInfoForm:customerCreateTicketForm:bildirimAciklama")
public WebElement aciklama;

@FindBy(id="emailGonderReplyForm:subject")
public WebElement subject;

@FindBy(id="customerOperationsForm:customerInfoForm:gonderId")
public WebElement gonder;

@FindBy(id="ticketInformationForm:customerMailLinkId")
public WebElement musteriyeEmail;

@FindBy(id="customerOperationsForm:customerInfoForm:customerCreateTicketForm:musteriTalepBildirimTarihPopupButton")
public WebElement talepBildirimTarihiCalendarButton;


@FindBy(id="ticketInformationForm:emailTeknikImage")
public WebElement teknipEkip;


@FindBy(id="ticketInformationForm:updateTmh")
public WebElement guncelle;


@FindBy(xpath="//*[text()='Bug�n']")
public WebElement bugun;

@FindBy(xpath="//*[contains(text(),'Talep T�r�')]/following::select[1]")
public WebElement talepTuru;

@FindBy(xpath="//*[contains(text(),'Firma Ad�')]/following::select[1]")
public WebElement firmaAdi;

@FindBy(xpath="//label[contains(text(),'�r�n Hizmet')]/following::select[1]")
public WebElement urunHizmet;

@FindBy(xpath="//*[contains(text(),'�l')]/following::select[1]")
public WebElement Il;

@FindBy(xpath="//*[contains(text(),'Telekom M�d�rl�k')]/following::select[1]")
public WebElement mudurluk;

@FindBy(xpath="//input[@value = 'Geri']")
public WebElement geris;

@FindBy(id="ticketInformationForm:bildirimGorevCikisButonu")
public WebElement ileri;
@FindBy(id="customerOperationsForm:customerInfoForm:customerCreateTicketForm:toggle_tmp_header")
public WebElement d;
@FindBy(id="ticketInformationForm:j_id465")
public WebElement islem;

@FindBy(xpath="//*[contains(text(),'M��teri Ad� Di�er')]/following::input[1]")
public WebElement musteriAdidiger;
//Numaral� Bildirim olu�turulmu�tur.

@FindBy(xpath="//*[contains(text(),'Numaral� Bildirim olu�turulmu�tur.')]")
public WebElement successResult;

@FindBy(id="customerOperationsForm:customerInfoForm:customerCreateTicketForm:musteriTalepBildirimTarihInputDate")
public WebElement talepBildirimDate;

@FindBy(xpath="//table[@id='customerOperationsForm:customerInfoForm:bildirimGonderPanelGridConfirmation']/tbody/tr[2]/td/div/input")
public WebElement tamam;
////*[@id="messages"]/li

@FindBy(xpath="//*[@id='messages']/li")
public WebElement bildirimNo;

public BildirimIslemleri(WebDriver driver)	{
	this.driver=driver;
	wait =new WebDriverWait(driver,20);
}	



	
public String manuelBildirim() throws InterruptedException, AWTException, FileNotFoundException, IOException
{//

	JavascriptExecutor js = (JavascriptExecutor) driver;
	pickToSelectText(gelisKanali, getConf("gelisKanali"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(birimAdi, getConf("birimAdi"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(grupAdi, getConf("grupAdi"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(talepTuru, getConf("talepTuru"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(firmaAdi, getConf("firmaAdi"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(urunHizmet, getConf("urunHizmet"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(Il, getConf("il"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(mudurluk, getConf("mudurluk"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));

	beklemeSuresiBelirt(1500);
	write(hizmetNo,"3122300156");
	click(bildirimBilgileri);
	beklemeSuresiBelirt(1000);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));

	click(bildirimBilgileri);
	click(musteriIslemleriEkrani);
	beklemeSuresiBelirt(1000);
	js.executeScript("arguments[0].click();", talepBildirimTarihiCalendarButton);
	//click(talepBildirimTarihiCalendarButton);
	beklemeSuresiBelirt(1000);
	js.executeScript("arguments[0].click();", bugun);
	//js.executeScript("document.body.style.zoom='80%'");
	beklemeSuresiBelirt(1500);//
	click(musteriIslemleriEkrani);
	click(aciklamalarLink);
	write(aciklama, "sasa");
	beklemeSuresiBelirt(1000);
	
	js.executeScript("arguments[0].click();", aciklamalarLink);
//mahir
	
	
	beklemeSuresiBelirt(2000);
	//js.executeScript("document.body.style.zoom='100%'");
	click(gonder);
	click(tamam);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	xpathParcasiMesajKontrolEt(successResult, "Numaral� Bildirim olu�turulmu�tur.");
	String bildirim=metniAl(bildirimNo);
	bildirim=bildirim.substring(0,9);
	return bildirim;
	//degisiklik yapildi.saa
}


public void manuelBildirimAttachment() throws Exception
{
	
	JavascriptExecutor js = (JavascriptExecutor) driver;
	pickToSelectText(gelisKanali, getConf("gelisKanali"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(birimAdi, getConf("birimAdi"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(grupAdi, getConf("grupAdi"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(talepTuru, getConf("talepTuru"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(firmaAdi, getConf("firmaAdi"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(urunHizmet, getConf("urunHizmet"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(Il, getConf("il"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(mudurluk, getConf("mudurluk"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));

	beklemeSuresiBelirt(1500);
	write(hizmetNo,"3122300156");
	click(bildirimBilgileri);
	beklemeSuresiBelirt(1000);
	click(bildirimBilgileri);
	click(musteriIslemleriEkrani);
	beklemeSuresiBelirt(1000);
	js.executeScript("arguments[0].click();", talepBildirimTarihiCalendarButton);
	//click(talepBildirimTarihiCalendarButton);
	beklemeSuresiBelirt(1000);
	js.executeScript("arguments[0].click();", bugun);
	//js.executeScript("document.body.style.zoom='80%'");
	beklemeSuresiBelirt(1500);//
	click(musteriIslemleriEkrani);
	click(aciklamalarLink);
	write(aciklama, "sasa");
	beklemeSuresiBelirt(1000);
	
	js.executeScript("arguments[0].click();", aciklamalarLink);
//mahir
	
	click(dosyalarLink);
	click(dosyaEkle);
	beklemeSuresiBelirt(1500);
	Screen sc=new Screen();
	sc.wait("images/1497663267067.png");
	sc.type("images/1497662116939.png", "1.png");
	sc.click("images/1497662153788.png");
	beklemeSuresiBelirt(1000);
	click(yukle);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	beklemeSuresiBelirt(1000);
	click(dosyalarLink);
	beklemeSuresiBelirt(2000);
	//js.executeScript("document.body.style.zoom='100%'");
	click(gonder);
	click(tamam);
	//js.executeScript("arguments[0].click();", gonder);
	//js.executeScript("arguments[0].click();", tamam);
	xpathParcasiMesajKontrolEt(successResult, "Numaral� Bildirim olu�turulmu�tur.");
	metniAl(bildirimNo);
	//degisiklik yapildi.saa
//	java.awt.Toolkit.getDefaultToolkit().getSystemClipboard().setContents(Sec, null);
//	
//	                           Robot robot = new Robot();
//	                           robot.keyPress(KeyEvent.VK_ENTER);
//	                           robot.keyRelease(KeyEvent.VK_ENTER);
//	                           robot.keyPress(KeyEvent.VK_CONTROL);
//	                           robot.keyPress(KeyEvent.VK_V);
//	                           robot.keyRelease(KeyEvent.VK_V);
//	                           robot.keyRelease(KeyEvent.VK_CONTROL);
//	                           robot.keyPress(KeyEvent.VK_ENTER);
//	                           robot.keyRelease(KeyEvent.VK_ENTER);
//	                           
}

//	                 



public void bildiriGuncelle() throws InterruptedException, FileNotFoundException, IOException
{
	pickToSelectText(firmaAdi, "DO�AN TV D�G�TAL PLATFORM ��LETMEC�L��� A�.");
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(urunHizmet, getConf("urunHizmet"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(Il, getConf("il"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	pickToSelectText(mudurluk, getConf("mudurluk"));
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));

	beklemeSuresiBelirt(1000);
	click(bildirimBilgileri);
	/*
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("document.body.style.zoom='80%'");
	beklemeSuresiBelirt(2000);//
	js.executeScript("arguments[0].click();", bildirimIslemleriLink);
	beklemeSuresiBelirt(2000);
	js.executeScript("arguments[0].click();", guncelle);
	beklemeSuresiBelirt(3000);
	js.executeScript("arguments[0].click();", geris);

	js.executeScript("document.body.style.zoom='100%'");*/
	click(bildirimIslemleriLink);
	beklemeSuresiBelirt(1000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", guncelle);
	beklemeSuresiBelirt(1000);
	click(bildirimIslemleriLink);
	click(geris);
}

public void teyideAl() throws InterruptedException
{
	click(bildirimBilgileri);
	click(bildirimIslemleriLink);
	pickToSelectText(islemCombo, "Teyide Al");
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	click(ileri);
	
	pickToSelectText(gorevCikisCombo, "Teyit");

	/*
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("document.body.style.zoom='80%'");
	beklemeSuresiBelirt(2000);//
	js.executeScript("arguments[0].click();", bildirimIslemleriLink);
	beklemeSuresiBelirt(2000);
	pickToSelectText(islemCombo, "Teyide Al");
	//js.executeScript("arguments[0].click();", guncelle);
	beklemeSuresiBelirt(3000);
	js.executeScript("arguments[0].click();", islem);
	beklemeSuresiBelirt(3000);
	pickToSelectText(gorevCikisCombo, "Teyit");
	beklemeSuresiBelirt(3000);
	js.executeScript("arguments[0].click();", gorevCikisiPanelButton);
	js.executeScript("document.body.style.zoom='100%'");
	*/
	
}



@FindBy(xpath="//*[contains(text(),'Bildirim BackOffice Ekibine Atanmalidir')]")
public WebElement backOfficeAtaLabel;

@FindBy(xpath="//*[contains(text(),'G�nderilecek Birim')]/following::select[1]")
public WebElement gonderilecekBirim;


@FindBy(xpath="//*[contains(text(),'G�rev C�k�� Nedeni')]/following::select[1]")
public WebElement gorevCikisNedeni;

@FindBy(xpath="//*[contains(text(),'numar�l� bildirim havuza g�nderilmi�tir.')]")
public WebElement successResultAtamaBO;

public void backOfficeAta() throws InterruptedException
{
	xpathParcasiMesajKontrolEt(backOfficeAtaLabel, "Bildirim BackOffice Ekibine Atanmalidir");
	click(bildirimBilgileri);
	click(bildirimIslemleriLink);
	pickToSelectText(islemCombo, "BackOffice e Ata");
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	click(ileri);
	pickToSelectText(gonderilecekBirim, "ISS Backoffice");
	beklemeSuresiBelirt(1000);
	pickToSelectText(gorevCikisNedeni, "Seviye G�ncellendi");
	beklemeSuresiBelirt(1000);
	click(gerceklestir);
	xpathParcasiMesajKontrolEt(successResultAtamaBO, "numar�l� bildirim havuza g�nderilmi�tir.");
	//numar�l� bildirim havuza g�nderilmi�tir.
}

@FindBy(xpath="//*[contains(text(),'Bildirim Eskalasyon Ekibine Atanmalidir')]")
public WebElement eskalasyonAtaLabel;

public void eskalasyonAta() throws InterruptedException
{
	xpathParcasiMesajKontrolEt(eskalasyonAtaLabel, "Bildirim Eskalasyon Ekibine Atanmalidir");
	click(bildirimBilgileri);
	click(bildirimIslemleriLink);
	pickToSelectText(islemCombo, "Eskalasyon a Ata");
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	click(ileri);
	pickToSelectText(gonderilecekBirim, "ISS Eskalasyon");
	beklemeSuresiBelirt(1000);
	pickToSelectText(gorevCikisNedeni, "Seviye G�ncellendi");
	beklemeSuresiBelirt(1000);
	click(gerceklestir);
	xpathParcasiMesajKontrolEt(successResultAtamaBO, "numar�l� bildirim havuza g�nderilmi�tir.");
	//numar�l� bildirim havuza g�nderilmi�tir.
}

public void bildirimKapama(String bildirimNo,String kapamaGrub,String kapamaKodus) throws Exception
{
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("document.body.style.zoom='80%'");
	beklemeSuresiBelirt(2000);//
	js.executeScript("arguments[0].click();", bildirimIslemleriLink);
	beklemeSuresiBelirt(2000);
	pickToSelectText(islemCombo, "Bildirimi Kapat");
	//js.executeScript("arguments[0].click();", guncelle);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	
	js.executeScript("arguments[0].click();", ileri);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	js.executeScript("document.body.style.zoom='100%'");
	pickToSelectText(kapamaKoduGrubuCombo, kapamaGrub);
	beklemeSuresiBelirt(2000);
	pickToSelectText(kapamaKodu, kapamaKodus);
	beklemeSuresiBelirt(1000);
	click(gerceklestir);
	
	//Db den kapatilip kapatilmad�gi ve arch tablosuna atilma kontrolleri
	
	beklemeSuresiBelirt(5000);
	int bildirim=Integer.parseInt(bildirimNo);
	
	if(kapamaKontrol(bildirim)==0)
		ATUReports.add("Bildirim kapat�lm��t�r.", "KAPALI", "", "PASS", false);
	else
		ATUReports.add("Bildirim kapanmam��.", "Hata", "", "FAIL", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
	
	
	
	if(kapamaKontrolArch(bildirim)==1)
		ATUReports.add("Bildirim ar�ive at�lm��t�r.", "ar�ivde", "", "PASS", false);
	else
		ATUReports.add("Bildirim ar�ive at�lamad�.", "Hata", "", "FAIL", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
}

@FindBy(id="emailGonderTeknikEkipForm:toTeknikEkip")
public WebElement toSaha;

@FindBy(id="emailGonderTeknikEkipForm:subjectTeknikEkip")
public WebElement subjectSaha;


@FindBy(xpath="//input[contains(@id,'emailGonderTeknikEkipForm:j_id')]")
public WebElement mailGonderTeknik;

public void icIletisimSaha(String subject) throws InterruptedException, FileNotFoundException, IOException
{
	click(bildirimBilgileri);
	
	
	JavascriptExecutor js = (JavascriptExecutor) driver;
	
	
	//js.executeScript("document.body.style.zoom='60%'");
	beklemeSuresiBelirt(2000);//
	click(iletisimGecmisiLink);
	//js.executeScript("arguments[0].click();", iletisimGecmisiLink);
	beklemeSuresiBelirt(2000);
	//js.executeScript("arguments[0].click();",musteriyeEmail);
	click(teknipEkip);
	
	beklemeSuresiBelirt(3000);
	/*
	Random r = new Random();
	String s = generateString(r, "1234567890", 4);	
	s="test"+s;
	beklemeSuresiBelirt(1000);
	/*/
	write(toSaha, getConf("mailSaha"));
	beklemeSuresiBelirt(1000);
	write(subjectSaha, subject);
	beklemeSuresiBelirt(1000);
	click(mailGonderTeknik);
	//js.executeScript("arguments[0].click();",mailGonder);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	click(iletisimGecmisiLink);
	click(geris);
	//js.executeScript("arguments[0].click();", geris);

	//js.executeScript("document.body.style.zoom='100%'");
}

@FindBy(xpath="//td[contains(@id,'ticketInformationForm:customerticketConversationDataTable2:0:j')]")
public WebElement sonMailHover;
public void iciletisimManuel() throws InterruptedException
{
	click(bildirimBilgileri);
	//JavascriptExecutor js = (JavascriptExecutor) driver;
	//js.executeScript("document.body.style.zoom='60%'");
	//beklemeSuresiBelirt(2000);//
	click(iletisimGecmisiLink);
	//js.executeScript("arguments[0].click();", iletisimGecmisiLink);
	beklemeSuresiBelirt(2000);
	//js.executeScript("arguments[0].click();",musteriyeEmail);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	click(sonMailHover);
	click(musteriyeEmail);
	beklemeSuresiBelirt(3000);
	//write(to, "kapomaho@gmail.com");
	beklemeSuresiBelirt(2000);
	click(mailGonder);
//	js.executeScript("arguments[0].click();",mailGonder);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	//js.executeScript("arguments[0].click();", geris);
	click(iletisimGecmisiLink);
	click(geris);
	//js.executeScript("document.body.style.zoom='100%'");
}
	
}
