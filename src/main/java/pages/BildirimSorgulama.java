package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class BildirimSorgulama extends base.AltYapi{
	
public WebDriver driver;
public WebElement element;


@FindBy(xpath="//*[contains(text(),'A�ama')]")
public WebElement asama;

@FindBy(id="ticketSearchForm:kullaniciKoduId")
public WebElement bildirimNoArama;

@FindBy(id="ticketSearchForm:submitButon")
public WebElement araButton;

public JavascriptExecutor js;
public BildirimSorgulama(WebDriver driver)	{
	this.driver=driver;
	 js = (JavascriptExecutor) driver;
	wait =new WebDriverWait(driver,50);
}	


public void bildirimArama(int bildirim) throws InterruptedException
{
	xpathParcasiMesajKontrolEt(asama, "A�ama");

	write(bildirimNoArama, String.valueOf(bildirim));
	click(araButton);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));

}

public String talepDurumuKontrolEt(int bildirim,String status) throws InterruptedException
{

	bildirimArama(bildirim);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(),'"+bildirim+"')]")));
	element=driver.findElement(By.xpath("//*[contains(text(),'"+bildirim+"')]/following::td[8]")); //Talep durumu gosterir
	beklemeSuresiBelirt(1000);
	String durums=element.getText();
	System.out.println(durums);
	/*
	String durums=durum.getText();
	System.out.println(durums);
	if(durums.compareTo(status)==0)
	{
		ATUReports.add(status, "statu dogru", "", "PASS", false);

	}
	else
	{
		ATUReports.add(status, "hata", "", "FAIL", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

	}
	*/
	return durums;
}



}
