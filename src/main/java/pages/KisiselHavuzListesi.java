package pages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class KisiselHavuzListesi extends base.AltYapi{
	
public WebDriver driver;
public WebElement element;

@FindBy(id="loginForm:username")
public WebElement userName;

@FindBy(xpath="//td[contains(@id,'talepDurumuColumn')]")
public WebElement durum;

@FindBy(id="kisiselHavuzListesiForm4:taskKslTable:refreshKisiselHavuzList")
public WebElement refresh;


@FindBy(xpath="//*[contains(text(),'Talep Durumu')]")
public WebElement talepDurumu;

@FindBy(xpath="//*[contains(@id,'bildirimNoIdKisisel')]")
public WebElement bildirimSearchbox;

public KisiselHavuzListesi(WebDriver driver)	{
	this.driver=driver;
	wait =new WebDriverWait(driver,50);
}	


public void bildirimArama(int bildirim) throws InterruptedException
{	
	xpathParcasiMesajKontrolEt(talepDurumu, "Talep Durumu");
	JavascriptExecutor js = (JavascriptExecutor) driver;

	write(bildirimSearchbox,String.valueOf(bildirim));
//	js.executeScript("document.body.style.zoom='60%'");
	beklemeSuresiBelirt(2000);//
	js.executeScript("arguments[0].click();", refresh);
	//click();
	beklemeSuresiBelirt(2000);
		
	
}
	
public String talepDurumuKontrolEt(int bildirim,String status) throws InterruptedException
{
	beklemeSuresiBelirt(2000);
	bildirimArama(bildirim);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(),'"+bildirim+"')]")));
	element=driver.findElement(By.xpath("//*[contains(text(),'"+bildirim+"')]/following::td[6]")); //Talep durumu gosterir
	beklemeSuresiBelirt(1000);
	String durums="";
	//System.out.println(durums);
	
	int i=0;
	
	while(i<180)
		
	{
		
		click(refresh);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
		durums=durum.getText();
		if(durums.compareTo(status)==0)
		{
			ATUReports.add(status, "statu dogru", "", "PASS", false);
			i=182;
		}
		
		beklemeSuresiBelirt(5000);
		
		
	}
	
	
	if(i==181)
		ATUReports.add(status, "hata", "", "FAIL", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

	
	
	return durums;
}

public BildirimIslemleri bildirimDetayGit(int bildirim) throws InterruptedException
{
	
	bildirimArama(bildirim);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	
	element=driver.findElement(By.xpath("//a[contains(text(),'"+bildirim+"')]")); //Talep durumu gosteri
	js.executeScript("arguments[0].click();", element);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(),'L�tfen Bekleyin...')]")));
	return PageFactory.initElements(driver, BildirimIslemleri.class);
}
}
