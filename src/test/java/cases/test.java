package cases;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.DBConnect;
import base.ReplyToEmail;
import base.SentMail;
import pages.BildirimIslemleri;
import pages.HomePage;
import pages.KisiselHavuzListesi;
import pages.LoginPage;
import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


@Listeners({ ATUReportsListener.class, ConfigurationListener.class,MethodListener.class })
public class test extends base.AltYapi{
	
	{
		String atuConfigLocation = String.format("%s%ssrc%smain%sresources%satu.properties",System.getProperty("user.dir"), File.separator, File.separator, File.separator,File.separator, File.separator);
		System.out.println("atuConfigLocation :" + atuConfigLocation);
		System.setProperty("atu.reporter.config", atuConfigLocation);
	}
		 
@Test
public void testSendMail()  throws Exception{

/*
	ReplyToEmail rep=new ReplyToEmail();
	rep.sendMail("mahir", "ada");*/
	/*
	DBConnect d=new DBConnect();
	System.out.println(d.getSubject(104999430));
	
	 * 
	 */
	String bildirimNo=getConfDepend("bildirimno");

	init("", "Login");	
	LoginPage lp=PageFactory.initElements(driver, LoginPage.class);	
	
	HomePage home=lp.doLogin("usernameAdmin","passwordAdmin");
	home.bildirimSorgulama().talepDurumuKontrolEt(Integer.parseInt(bildirimNo), "Kapal�");
	
	//home.navigateAtamasiYapilan().kisiAtama(105019859).kullaniciAtama();

}

@AfterMethod
public void testiSonlandir() throws InterruptedException, IOException {
	if(driver!= null) {
		//driver.quit();
	}
}

}