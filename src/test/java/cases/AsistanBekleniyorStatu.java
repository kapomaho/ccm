package cases;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.SentMail;
import pages.BildirimIslemleri;
import pages.HomePage;
import pages.KisiselHavuzListesi;
import pages.LoginPage;
import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


@Listeners({ ATUReportsListener.class, ConfigurationListener.class,MethodListener.class })
public class AsistanBekleniyorStatu extends base.AltYapi{
	
	{
		String atuConfigLocation = String.format("%s%ssrc%smain%sresources%satu.properties",System.getProperty("user.dir"), File.separator, File.separator, File.separator,File.separator, File.separator);
		System.out.println("atuConfigLocation :" + atuConfigLocation);
		System.setProperty("atu.reporter.config", atuConfigLocation);
	}
		 
@Test
public void testSendMail()  throws Exception{


	
	String bildirimNo=getConfDepend("bildirimno");
	String subject=getSubject(Integer.parseInt(bildirimNo));
	System.out.println("Bildirim no "+bildirimNo+"Subject"+subject);
	String mailUser=getConf("mailKullanici");
	String mailUserPassword=getConf("mailKullaniciSifre");
	System.out.println(mailUser+""+mailUserPassword);
	replyMail(mailUser,mailUserPassword,subject,"");
	//beklemeSuresiBelirt(45000);
	init("", "Login");	
	LoginPage lp=PageFactory.initElements(driver, LoginPage.class);	
	
	HomePage home=lp.doLogin("usernameAdmin","passwordAdmin");
	home.navigateKisiselHavuz();
	KisiselHavuzListesi kisi=PageFactory.initElements(driver, KisiselHavuzListesi.class);	
	//kisi.bildirimArama(Integer.parseInt(bildirimNo));
	kisi.talepDurumuKontrolEt(Integer.parseInt(bildirimNo),"Asistanda Bekliyor");
	
}

@AfterMethod
public void testiSonlandir() throws InterruptedException, IOException {
	if(driver!= null) {
		//driver.quit();
	}
}

}